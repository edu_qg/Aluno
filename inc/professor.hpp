#ifndef PROFESSOR_H
#define PROFESSOR_H

#include "pessoa.hpp"

	class Professor: public Pessoa{
	private:
		int matricula_func;
		string disciplinas;
		float salario;
		string projetos;
		
	public:
		Professor();
		Professor(string nome, 
			string idade, 
			string telefone, 
			int matricula_func, 
			string disciplinas, 
			float salario, 
			string projetos);

		void setMatriculaP(int matricula_func);
		int getMatriculaP();

		void setDisciplinas(string disciplinas);
		string getDisciplinas();

		void setSalario(float salario);
		float getSalario();

		void setProjetos(string projetos);
		string getProjetos();
};

#endif
