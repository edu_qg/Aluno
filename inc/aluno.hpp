#ifndef ALUNO_H
#define ALUNO_H

//primeiro arquivo do projeto
#include "pessoa.hpp"//include do header

class Aluno: public Pessoa 
{//para herdar a classe, aluno herda 
			  //classe pessoa
	private:
		int matricula;
		int quantidade_de_creditos;
		int semestre;
		float ira;
	public:
		Aluno();//posso deletar se quiser, para colocar nomes padrao caso nao seja colocado
		Aluno( string nome , string idade , string telefone , int matricula );
		
		void setMatricula( int matricula );
		int getMatricula ();

		void setQuantidadeCreditos( int creditos );
		int getQuantidadeCreditos();

		void setSemestre( int semestre );
		int getSemestre ();

		void setIra( float ira);
		float getIra();


};

#endif
