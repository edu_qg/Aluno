#include <iostream>
#include <string.h>
#include "aluno.hpp"

Aluno::Aluno(){//para predefinir

	setNome("");
	setIdade("");
	setTelefone("");
	setMatricula(0);
	setQuantidadeCreditos(0);
	setSemestre(0);
	setIra(0);
	
}

Aluno::Aluno(string nome , string idade , string telefone , int matricula){

	setNome(nome);
	setIdade(idade);
	setTelefone(telefone);
	setMatricula(matricula);
	setQuantidadeCreditos(quantidade_de_creditos);
	setSemestre(semestre);
	setIra(ira);

}
void Aluno::setMatricula( int matricula ){
	this->matricula = matricula;
}
int Aluno::getMatricula(){
	return matricula;
}
void Aluno::setQuantidadeCreditos( int creditos ){
	quantidade_de_creditos = creditos;
}
int Aluno::getQuantidadeCreditos(){
	return quantidade_de_creditos;
}
void Aluno::setSemestre(int semestre){
	this->semestre = semestre;
}
int Aluno::getSemestre(){
	return semestre;//para converter string em inteiro return "1 semestre de 2015"
}
void Aluno::setIra( float ira ){
	this->ira = ira;
}
float Aluno::getIra(){
	return ira;
}

