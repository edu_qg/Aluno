#include <iostream>
#include <string>
#include "professor.hpp"

Professor::Professor(){
	setNome("");
	setIdade("");
	setTelefone("");

	setMatriculaP(0);
	setSalario(0);
	setProjetos("");
	setDisciplinas("");
}
Professor::Professor(string nome,string idade,string telefone,int matricula_func,string disciplinas,float salario,string projetos){
	setNome(nome);
	setIdade(idade);
	setTelefone(telefone);
	setMatriculaP(matricula_func);
	setSalario(salario);
	setProjetos(projetos);
	setDisciplinas(disciplinas);
}
void Professor::setMatriculaP(int matricula_func){
	this->matricula_func = matricula_func;
}
int Professor::getMatriculaP(){
	return matricula_func;
}
//
void Professor::setDisciplinas(string disciplinas1){
	disciplinas = disciplinas1;
}
string Professor::getDisciplinas(){
	return disciplinas;
}
//
void Professor::setSalario(float salario){
	this->salario = salario;
}
float Professor::getSalario(){
	return salario;
}
//
void Professor::setProjetos(string projetos){
	this->projetos = projetos;
}
string Professor::getProjetos(){
	return projetos;
}
